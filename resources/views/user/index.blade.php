@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if(session()->has('success'))
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span
                        aria-hidden="true">×</span>
                </button>
            </div>
        </div>
        @endif
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Admin Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <a href="{{route('user.create')}}" class="btn btn-success float-right mb-3">Create User</a>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">User ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">User Type</th>
                                    <th scope="col">Register Date</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($user as $u)
                                <tr>
                                    <th scope="row">{{(($user->currentPage()-1) * $user->perPage()) + $loop->iteration}}
                                    </th>
                                    <td>{{$u->id}}</td>
                                    <td>{{$u->name}}</td>
                                    <td>{{$u->email}}</td>
                                    <td>{{$u->getRole()}}</td>
                                    <td>{{$u->created_at}}</td>
                                    <td>
                                        <form method="POST" action="{{route('user.destroy', $u->id)}}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <a href="{{route('user.edit', $u->id)}}" class="btn btn-warning">Edit</a>
                                            <button type="submit" class="btn btn-danger pull-right"
                                                onclick="return confirm('Are you sure you want to delete this user?')">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{$user->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
