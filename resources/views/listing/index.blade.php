@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if(session()->has('success'))
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span
                        aria-hidden="true">×</span>
                </button>
            </div>
        </div>
        @endif
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Listing Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <a href="{{route('listing.create')}}" class="btn btn-success float-right mb-3">Create Listing</a>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">List ID</th>
                                    <th scope="col">List Name</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Latitude</th>
                                    <th scope="col">Longitude</th>
                                    <th scope="col">Submitter ID</th>
                                    <th scope="col">Date Created</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($listing as $l)
                                <tr>
                                    <th scope="row">
                                        {{(($listing->currentPage()-1) * $listing->perPage()) + $loop->iteration}}</th>
                                    <td>{{$l->id}}</td>
                                    <td>{{$l->list_name}}</td>
                                    <td>
                                        <pre class="prestyle">{{$l->address}}</pre>
                                    </td>
                                    <td>{{$l->latitude}}</td>
                                    <td>{{$l->longitude}}</td>
                                    <td>{{$l->submitter_id}}</td>
                                    <td>{{$l->created_at}}</td>
                                    <td>
                                        <form method="POST" action="{{route('listing.destroy', $l->id)}}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <a href="{{route('listing.edit', $l->id)}}" class="btn btn-warning">Edit</a>
                                            <button type="submit" class="btn btn-danger pull-right"
                                                onclick="return confirm('Are you sure you want to delete this listing?')">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{$listing->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
