<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller
{

    public function login()
    {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')]))
        {
            $user = Auth::user();
            $token =  $user->createToken('listingToken')-> accessToken;

            return response()->json([
                'user_id' => $user->id,
                'token' => $token,
                'status' => [
                    'code' => 200,
                    'message' => 'Access Granted'
                ]
            ]);
        }
        else
        {
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

}
