<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Listing;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        $user_id = Input::get('user_id');
        $clientlatitude = Input::get('latitude');
        $clientlongitude = Input::get('longitude');
        //Filter listing by requested user_id
        $listing = Listing::where('submitter_id', $user_id)->get();
        $listingdistance = [];

        foreach($listing as $l)
        {
            //Calculate distance between current mobile location and listing location
            $listinglatitude = $l->latitude;
            $listinglongitude = $l->longitude;
            $theta = $clientlongitude - $listinglongitude;
            $distance = sin(deg2rad($clientlatitude)) * sin(deg2rad($listinglatitude)) +  cos(deg2rad($clientlatitude)) * cos(deg2rad($listinglatitude)) * cos(deg2rad($theta));
            $distance = acos($distance);
            $distance = rad2deg($distance);
            //Convert distance to km
            $distanceinkm = $distance * 60 * 1.1515 * 1.609344;
            //Round distance to 3 decimal points
            $distanceinkm = number_format($distanceinkm, 3, '.', '');

            $listingdistance[] = [
                'id' => $l->id,
                'list_name' => $l->list_name,
                'distance' => $distanceinkm
            ];
        }

        return response()->json([
            'listing' => $listingdistance,
            'status' => [
                'code' => 200,
                'message' => 'Listing successfully retrieved'
            ]
        ]);
    }

}
