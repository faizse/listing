<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Listing extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'list_name', 'address', 'latitude', 'longitude', 'submitter_id'
    ];

    public function submitter()
    {
        return $this->belongsTo('App\User');
    }
}
