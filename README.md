## How to run the project
Follow this step to run the project

1. Clone the repository
2. Run `composer install` command
3. Run `cp .env.example .env` command
4. Run `php artisan key:generate` command
5. Setup the database configuration in .env
6. Run `php artisan migrate` command
7. Run `php artisan passport:install` command
8. Run `php artisan serve` command
9. Register administrator at `yoursite.com/register`
10. System is ready to use

---

## API Module URL
Use these URLs to test the API module in Postman or other API testing tool

1. User Login (POST) 
    - URL: `yoursite.com/api/login`
    - Specify email and password in the request body editor
2. Listing Module (GET) 
    - URL: `yoursite.com/api/listing?user_id=*yourvalue*&token=*yourvalue*&latitude=*yourvalue*&longitude=*yourvalue*`
    - Specify access token as a Bearer token for Authorization in the request header editor